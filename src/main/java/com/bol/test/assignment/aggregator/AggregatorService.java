package com.bol.test.assignment.aggregator;

import com.bol.test.assignment.offer.Offer;
import com.bol.test.assignment.offer.OfferCondition;
import com.bol.test.assignment.offer.OfferService;
import com.bol.test.assignment.order.Order;
import com.bol.test.assignment.order.OrderService;
import com.bol.test.assignment.product.Product;
import com.bol.test.assignment.product.ProductService;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;

public class AggregatorService {
    private OrderService orderService;
    private OfferService offerService;
    private ProductService productService;
    private Offer offer;
    private Product product;
    
    private Callable<Offer> callableOffer(Order order){
        return () -> {
            try {
                return offerService.getOffer(order.getOfferId());
            }
            catch (RuntimeException e) {
                return null;
            }
        };
    }
    
    private Callable<Product> callableProduct(Order order){
        return () -> {
            try {
                return productService.getProduct(order.getProductId());
            }
            catch (RuntimeException e) {
                return null;
            }
        };
    }

    public AggregatorService(OrderService orderService, OfferService offerService, ProductService productService) {
        this.orderService = orderService;
        this.offerService = offerService;
        this.productService = productService;
    }
    
    /**
    * Returns an enriched order. Offer and Product details
    * are included if the Offer and Product Services are available and
    * give a response within the timeframe.
    *
    * @param sellerId For some reason we use the seller id to fectch
    * the order.
    */
    public EnrichedOrder enrich(int sellerId) throws ExecutionException, InterruptedException {
        
        Order order = orderService.getOrder(sellerId);
        
        retrieveOfferAndProductInformation(order);
        
        EnrichedOrder enrichedOrder = new EnrichedOrder(order.getId());
        
        if(offer != null){
            enrichedOrder.enrichWithOfferDetails(offer.getId(), offer.getCondition());
        }
        if(product != null){
            enrichedOrder.enrichWithProductDetails(product.getId(), product.getTitle());
        }
        
        return enrichedOrder;
    }
    
    private void retrieveOfferAndProductInformation(Order order) throws ExecutionException, InterruptedException {
        ExecutorService es = Executors.newCachedThreadPool();
        
        Future<Offer> futureOffer = es.submit(callableOffer(order));
        Future<Product> futureProduct = es.submit(callableProduct(order));
        try{
            offer = futureOffer.get(1700, TimeUnit.MILLISECONDS);
        }
        catch(TimeoutException toe){
            //Use logger when in production
            System.err.println("The Offer service request timed-out");
        }
        try{
            product = futureProduct.get(1700, TimeUnit.MILLISECONDS);
        }
        catch(TimeoutException toe){
            //Use logger when in production
            System.err.println("The Product service request timed-out");
        }
    }
}
