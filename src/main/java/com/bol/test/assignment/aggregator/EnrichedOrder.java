package com.bol.test.assignment.aggregator;

import com.bol.test.assignment.offer.OfferCondition;

public class EnrichedOrder {
    private int id;
    private int offerId;
    private OfferCondition offerCondition;
    private int productId;
    private String productTitle;
    
    public EnrichedOrder(int id) {
        this.id = id;
        this.offerId = -1;
        this.offerCondition = OfferCondition.UNKNOWN;
        this.productId = -1;
        this.productTitle = null;
    }

    public int getId() {
        return id;
    }

    public int getOfferId() {
        return offerId;
    }

    public OfferCondition getOfferCondition() {
        return offerCondition;
    }

    public int getProductId() {
        return productId;
    }

    public String getProductTitle() {
        return productTitle;
    }
    
    public void enrichWithOfferDetails(int offerId, OfferCondition offerCondition){
        this.offerId = offerId;
        this.offerCondition = offerCondition;
    }
    
    public void enrichWithProductDetails(int productId, String title){
        this.productId = productId;
        this.productTitle = title;
    }
    
}
